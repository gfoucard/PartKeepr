#!/bin/sh

# Adopt the uid of the php user to the uid of the outer world.
sed '/www-data/s@www-data:x:\([0-9]*\)@www-data:x:'"$GITHUB_DEBUG_UID@" -i /etc/passwd

if [ ! -d /var/www/pk ]; then
	cp -ra /var/template/. /var/www/.
fi

cd /var/www/pk

# Allow www-data to use composer
# This must be done after the uid of www-data has been alterd.
mkdir /var/www/.composer && chown www-data: /var/www/.composer

if [ "$1" = "docker-php-entrypoint" ]; then
	# Override config
	grep -v "setParameter('database_" app/config/parameters.php.dist > app/config/parameters.php
	cat /etc/app/parameters.defaults.php >> app/config/parameters.php

	exec "$@"
else
	echo "Calling manual command $@ as user www-data"
	
	exec "$@"
fi
